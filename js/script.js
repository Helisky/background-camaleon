var button = document.getElementById('btn');
var hoverTimeout;


// get random color from object
function getRandomColor(){
    color = dataColors[Math.floor(Math.random()*dataColors.length)];
    if(color !== undefined){
        setValues(color.name,color.hex, color.rgb);
    }
}

function setValues(colorName, rgb, hx) {
    document.body.style.backgroundColor = hx;
    document.getElementById("color-name").innerHTML = colorName;
    document.getElementById("color-rgb").innerHTML = rgb;
    document.getElementById("color-hex").innerHTML = hx;
}


//   hover
function activateHover() {
    button.classList.add('hover');
    hoverTimeout = setTimeout(function() {
      getRandomColor();
    }, 800);
  }
  
  function deactivateHover() {
    hoverTimeout = setTimeout(function() {
      clearTimeout(hoverTimeout);
      button.classList.remove('hover');
    }, 800); 
  }
  
//   clic
  button.addEventListener('mousedown', activateHover); 
  document.addEventListener('mouseup', function() {
    deactivateHover();
  });
  

//   space 
  document.addEventListener('keydown', function(event) {
    if (event.key === ' ') {
      activateHover();
    }
  });
  
  document.addEventListener('keyup', function(event) {
    if (event.key === ' ') {
      deactivateHover();
    }
  });
  
